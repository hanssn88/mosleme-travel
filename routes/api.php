<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//jamaah apps
Route::prefix('jamaah')->group(function () {
    Route::post('/register', 'Jamah\JamaahController@register');
    Route::post('/login', 'Jamah\JamaahController@login');
    Route::post('/get_cabang', 'CabangController@index');
});


//agen apps
Route::prefix('agen')->group(function () {
    Route::post('/register', 'Agen\AgenController@register');
    Route::post('/login', 'Agen\AgenController@login');
});


//admin
Route::middleware('checkToken')->prefix('admin')->group(function () {

    //Data Cabang
    Route::post('/get_cabang', 'CabangController@index');
    Route::post('/save_cabang', 'CabangController@store');
    Route::get('/cabang/{id}', 'CabangController@detail');
    Route::delete('/cabang/{id}', 'CabangController@remove');

    //Data Akomodasi
    Route::post('/get_akomodasi', 'AkomodasiController@index');
    Route::post('/save_akomodasi', 'AkomodasiController@store');
    Route::get('/akomodasi/{id}', 'AkomodasiController@detail');
    Route::delete('/akomodasi/{id}', 'AkomodasiController@remove');
});


//sample
Route::get('/genToken', 'CabangController@testToken');
Route::middleware('checkToken')->group(function () {
    Route::get('/getDataToken', 'CabangController@getDataToken');
});
