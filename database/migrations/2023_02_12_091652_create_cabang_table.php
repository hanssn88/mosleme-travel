<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cabang', function (Blueprint $table) {
            $table->id("id_cabang");
            $table->char('name', 100);
            $table->text('img')->nullable();
            $table->text('url_website')->nullable();
            $table->char('whatsapp', 18)->nullable();
            $table->char('device_key_wa', 64)->nullable();
            $table->char('color', 10)->nullable();
            $table->smallInteger('is_pusat', 1)->nullable()->default(0);
            $table->char('province', 64)->nullable();
            $table->char('district_or_city', 64)->nullable();
            $table->longText('address_details')->nullable();
            $table->char('latitude', 32)->nullable();
            $table->char('longitude', 32)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cabang');
    }
};
