<?php

namespace App\Repositories;

use App\Models\Cabang;

class CabangRepository extends BaseRepository
{

    public function __construct(Cabang $model)
    {
        parent::__construct($model);
    }

}
