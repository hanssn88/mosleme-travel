<?php

namespace App\Http\Controllers\Jamaah;

use App\Http\Controllers\Controller;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;

class JamaahController extends Controller
{
    //
    use ResponseAPI;

    public function __construct()
    {

    }


    //table jamaah


    public function register(Request $request)
    {

    }

    public function verifyEmail(Request $request)
    {

    }

    public function sendOtpRegisterWA(Request $request)
    {

    }

    public function resendOTPRegisterWA(Request $request)
    {

    }

    public function verifyOTPRegisterWA(Request $request)
    {

    }

    public function login(Request $request)
    {

    }

    public function loginSocialMedia(Request $request)
    {

    }

    public function verifyOTPLoginWA(Request $request)
    {

    }

    public function detail(Request $request)
    {

    }

    public function editProfile(Request $request)
    {

    }

    public function uploadPhoto(Request $request)
    {

    }

    public function changePassword(Request $request)
    {

    }

    public function sendLinkForgotPassword(Request $request)
    {

    }

    public function verifyForgotPassword(Request $request)
    {

    }


}
