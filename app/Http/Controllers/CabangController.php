<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Repositories\CabangRepository;
use App\Traits\ResponseAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CabangController extends Controller
{
    //
    use ResponseAPI;

    public function __construct(CabangRepository $cabangRepo)
    {
        $this->cabangRepo = $cabangRepo;
    }

    public function index(Request $request)
    {
        $per_page = (int)$request->per_page > 0 ? (int)$request->per_page : 0;
        $keyword = !empty($request->keyword) ? strtolower($request->keyword) : '';
        $sort_column = !empty($request->sort_column) ? $request->sort_column : 'name';
        $sort_order = !empty($request->sort_order) ? $request->sort_order : 'ASC';
        $page_number = (int)$request->page_number > 0 ? (int)$request->page_number : 1;

        $where = array();
        $count = $this->cabangRepo->countData($where);
        $data = [];
        if ($count > 0) {
            if (!empty($keyword)) {
                $data = $this->cabangRepo->searchData($where, $sort_column, $sort_order, "name", $keyword);
            } else {
                $per_page = $per_page > 0 ? $per_page : $count;
                $offset = ($page_number - 1) * $per_page;
                $data = $this->cabangRepo->getAllData($where, (int)$per_page, (int)$offset, $sort_column, $sort_order);
            }
        }
        return $this->success("ok", $data, 200, count($data));
    }

    public function detail(Request $request)
    {
        $data = $this->cabangRepo->find($request->id);
        return $this->success("ok", $data, 200, 1);
    }

    public function store(Request $request)
    {
        $id = (int)$request->id > 0 ? (int)$request->id : 0;
        $path_img = $request->file("images");
        if (!empty($path_img)) {
            $tujuan_upload = 'uploads/cabang';
            $filename = Helper::uploadImage($path_img, $tujuan_upload);
            $request->request->add(['img' => $filename]);
        }
        return $this->cabangRepo->CreateOrUpdate($request->all(), $id);
    }

    public function remove(Request $request)
    {
        $id = (int)$request->id > 0 ? (int)$request->id : 0;
        return $this->cabangRepo->delete($id, $request->operator_by);
    }

    public function testToken()
    {
        $dt = Crypt::encryptString(strtolower(1) . 'Þ' . "USR_AGEN" . 'Þ' . date('YmdHis'));
        return $this->success("ok", $dt, 200, 1);
    }

    public function getDataToken(Request $request)
    {
        return $this->success("ok", $request, 200, 1);
    }


}
