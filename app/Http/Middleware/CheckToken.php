<?php

namespace App\Http\Middleware;

use App\Traits\ResponseAPI;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */

    use ResponseAPI;

    public function handle(Request $request, Closure $next)
    {
        $header = $request->header('Authorization');
        $header = explode(" ", $header);
        if (empty($header[1])) {
            return $this->error('unauthorized', 401);
        }

        $request->operator_by = 1;
        $request->payloads = Crypt::decryptString($header[1]);
        return $next($request);
    }
}
