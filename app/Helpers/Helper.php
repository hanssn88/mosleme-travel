<?php

namespace App\Helpers;

class Helper
{

    static function uploadImage($path_img, $tujuan_upload)
    {
        $_tgl = date('YmdHi');
        $randomletter = substr(str_shuffle("MOSLEMEmoslemeTRAVELtravel"), 0, 13);
        $nama_file = base64_encode($_tgl . "" . $randomletter);
        $extension = $path_img->getClientOriginalExtension();
        $imageName = $nama_file . '.' . $extension;
        $path_img->move($tujuan_upload, $imageName);
        return !empty($imageName) ? env('PUBLIC_URL') . '/' . $tujuan_upload . '/' . $imageName : "";
    }


}
