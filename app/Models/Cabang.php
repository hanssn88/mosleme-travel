<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cabang extends Model
{
    protected $table = 'cabang';

    protected $primaryKey = 'id_cabang';

    protected $fillable = [
        'name',
        'img',
        'url_website',
        'whatsapp',
        'color',
        'province',
        'district_or_city',
        'address_details',
        'latitude',
        'longitude',
        'device_key_wa',
        'is_pusat',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected $hidden = [
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
        'deleted_by',
    ];
    protected $dates = ['deleted_at'];

}
